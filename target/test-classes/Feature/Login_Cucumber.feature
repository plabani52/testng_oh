Feature: Verify OrangeHRM Login
  
  Author: Plabani

  Scenario: Verify Login page for OrangeHRM
    Given Enter URL
    When Enter Username "Admin" and Password "admin123"
    Then click on Login buttion

  Scenario Outline: Valid and Invalid Input Output
    Given Enter URL
    When user login with username "<username>"
    When user login with password "<password>"
    Then click on Login buttion

    Examples: 
      | username | password   |
      | Admin    | Plabani098 |
      | Plabani  | admin123   |
