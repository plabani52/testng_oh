package com.automation.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{
	@FindBy(xpath = "//b[text()='Admin']")
	private WebElement btnAdmin;

	@FindBy(xpath = "//a[@id='menu_admin_UserManagement']")
	private WebElement btnUserManagement;
	
	public void clickUserManagement() 
	{
	Actions act = new Actions(driver);
	act.moveToElement(btnAdmin).moveToElement(btnUserManagement).build().perform();
	}

}
