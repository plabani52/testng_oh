package com.automation.test;

import org.testng.annotations.Test;

public class TestNG_Priority {
	
	@Test
	public void Add()
	{
		System.out.println("Add");
	}
	
	@Test(priority=1)
	public void Multiplication()
	{
		System.out.println("Multi");
	}

	@Test(priority=2)
	public void Divi()
	{
		System.out.println("Divi");

	}
}
