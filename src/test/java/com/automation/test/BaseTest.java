package com.automation.test;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;

import com.automation.GenericUtils.DriverUtils;
import com.automation.GenericUtils.PropertyReader;

public class BaseTest {
	
	WebDriver driver;
	@BeforeMethod
	public void setUp() throws FileNotFoundException, IOException
	{
		DriverUtils.createDriver();
		 DriverUtils.getDriver().get("https://opensource-demo.orangehrmlive.com/");
		 PropertyReader.initProperty();
	}
	


}
