package com.automation.GenericUtils;



import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelData {
	public static String getdata(String filepath,String sheetname,int rn,int cn)
	{
		try
		{
			FileInputStream fis=new FileInputStream(filepath);
			Workbook wb=WorkbookFactory.create(fis);
			Row r=wb.getSheet(sheetname).getRow(rn);
			String Data=r.getCell(cn).getStringCellValue();
			return Data;
		}catch(Exception E)
		{
			return " ";
		}
	}
	public static int getRowcount(String filepath,String sheetname) throws EncryptedDocumentException, IOException
	{ try {
		FileInputStream fis=new FileInputStream(filepath);
		Workbook wb=WorkbookFactory.create(fis);
		int rc=wb.getSheet(sheetname).getLastRowNum();
		return rc;
	}catch(Exception E)
	{
		return 0;
	}
	
	}
	public static int getCellcount(String filepath,String sheetname,int rc) throws EncryptedDocumentException, IOException
	{ try {
		FileInputStream fis=new FileInputStream(filepath);
		Workbook wb=WorkbookFactory.create(fis);
		int CC=wb.getSheet(sheetname).getRow(rc).getLastCellNum();
		return CC;
	}catch(Exception E)
	{
		return -1;
	}
	}

}
