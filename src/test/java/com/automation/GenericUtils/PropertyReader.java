package com.automation.GenericUtils;

import java.io.FileInputStream;
import java.util.Properties;

public class PropertyReader {
	static Properties prop = new Properties();

	public static void initProperty() {
		try {
			prop.load(new FileInputStream("C:\\Users\\Plabani\\eclipse-workspace\\TestNG_OH\\src\\test\\resources\\Config\\config.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getProperty(String key) {
		return prop.getProperty(key);
	}
}
