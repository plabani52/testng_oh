package com.automation.Stepdef;

import com.automation.GenericUtils.DriverUtils;
import com.automation.GenericUtils.PropertyReader;
import com.automation.page.HomePage;
import com.automation.page.LoginPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginStep {
	
	LoginPage lp=new LoginPage();
	HomePage hp=new HomePage();
	@Given("Open Browser")
	public void open_browser() {
	   DriverUtils.getDriver().get("https://opensource-demo.orangehrmlive.com/");
	}

	@When("Enter Username {string} and password {string}")
	public void enter_username_and_password(String string, String string2) {
	   lp.doLogin(string);
	   lp.doLogin1(string2);
	}

	@When("Click Login")
	public void click_login() {
	   lp.clickbtnLogin();
	}

	@Then("Login Successful")
	public void login_successful() {
		hp.clickUserManagement();
	}

	@When("Enter Username1 <{string}> and password1 <{string}>")
	public void enter_username1_and_password1(String string, String string2) {
		PropertyReader.initProperty();
	   lp.doLogin(PropertyReader.getProperty("Username"));
	   lp.doLogin1(PropertyReader.getProperty("Password"));
	}
	
	
	}

