package com.automation.Stepdef;

import com.automation.GenericUtils.DriverUtils;
import com.automation.page.LoginPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class OrangeHRMLoginScreenStep {

	LoginPage lp=new LoginPage();
	
		@Given("Enter URL")
		public void enter_url() {
		 DriverUtils.getDriver().get("https://opensource-demo.orangehrmlive.com/");  
		}

		@When("Enter Username {string} and Password {string}")
		public void enter_username_and_password(String string, String string2) {
		 lp.doLogin(string);
		 lp.doLogin1(string2);
		}

		@Then("click on Login buttion")
		public void click_on_login_buttion() {
		    lp.clickbtnLogin();
		}
		
		@When("user login with username {string}")
		public void user_login_with_username(String string) {
		 lp.doLogin(string);
		}

		@When("user login with password {string}")
		public void user_login_with_password(String string) {
		  lp.doLogin1(string);
		}
	}


